;(function() {
    'use strict';

    document.addEventListener('DOMContentLoaded', function() {

        function getCurrentTabUrl(callback) {

            var queryInfo = {
                active: true,
                currentWindow: true
            };

            chrome.tabs.query(queryInfo, function(tabs) {

                callback(tabs[0].url);
            });
        }

        getCurrentTabUrl(function(uri) {

            var targetChange = document.getElementById('last-change');
            var targetDates = document.getElementById('last-dates');
            var link = document.getElementById('link');
            var allow = false;
            var site = '';
            var goodId = '';

            if(/.*ulmart\.ru\/goods\/\d+.*/.test(uri)) {

                allow = true;
            }

            if(/.*citilink\.ru\/catalog\/.+?\/\d+.*/.test(uri)) {

                allow = true;
            }

            if(/.*dns-shop\.ru\/product\/\w+\/\w+.*/.test(uri)) {

                allow = true;
            }

            if(/.*mvideo\.ru\/products\/.*/.test(uri)) {

                allow = true;
            }

            if(allow) {

                site = 'shop';

                document.getElementById('banner').setAttribute('class', 'show');
                document.getElementById('prompt').setAttribute('class', 'hide');

                var d = new Date();
                var date = (d.getDate() + '/' + d.getMonth() + '/' + d.getYear());

                var linkText = '<a target="_blank" href="https://shop.sttv.tk/' + site + '/' + encodeURIComponent(uri) + '">Посмотреть полную статистику по этому товару</a>';

                if(localStorage['shop_helper_last_id'] == uri && localStorage['shop_helper_last_site'] == site && (localStorage['shop_helper_last_update'] == date)) {

                    targetDates.innerHTML = localStorage['shop_helper_response_dates'];
                    targetChange.innerHTML = localStorage['shop_helper_response_change'];

                    link.innerHTML = linkText;
                } else {

                    var data = {"id": uri, "allow": "chrome extension"};

                    var formData = new FormData();

                    for (var keyData in data) {

                        if (data.hasOwnProperty(keyData)) {

                            formData.append(keyData, data[keyData]);
                        }
                    }

                    var xhr = new XMLHttpRequest();
                    xhr.open("POST", "https://shop.sttv.tk/api/getGoodLastData", true);
                    xhr.onreadystatechange = function() {

                        if(xhr.readyState == 4) {

                            if (xhr.status == 200) {

                                var response = JSON.parse(xhr.responseText);

                                localStorage['shop_helper_last_site'] = site;
                                localStorage['shop_helper_last_id'] = goodId;
                                localStorage['shop_helper_last_update'] = date;
                                targetChange.innerHTML = localStorage['shop_helper_response_change'] = response.data;

                                if(response.dates) {

                                    targetDates.innerHTML = localStorage['shop_helper_response_dates'] = '<span>Сравнивались даты: <strong>' + response.dates[1] + '</strong> и <strong>' + response.dates[0] + '</strong></span>';
                                } else {

                                    targetDates.innerHTML = '';
                                }
                            } else {

                                targetDates.innerHTML = '';
                                targetChange.innerHTML = "<span class=\"error\">При запросе произошла ошибка. Возможно, сервер статистики временно недоступен. Попробуйте посмотреть информацию на сайте по ссылке ниже</span>";
                            }

                            link.innerHTML = linkText;
                        }
                    };

                    xhr.send(formData);
                }
            }

        });
    });
})();